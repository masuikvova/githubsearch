package com.masiuk.githubsearch.Models;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Organization extends RealmObject implements Parcelable {
    @SerializedName("login")
    private String login;
    @SerializedName("id")
    private String id;
    @SerializedName("avatar_url")
    private String avatar_url;
    @SerializedName("organizations_url")
    private String organizations_url;
    @SerializedName("repos_url")
    private String repos_url;

    public Organization() {
    }

    protected Organization(Parcel in) {
        login = in.readString();
        id = in.readString();
        avatar_url = in.readString();
        organizations_url = in.readString();
        repos_url = in.readString();
    }

    public static final Creator<Organization> CREATOR = new Creator<Organization>() {
        @Override
        public Organization createFromParcel(Parcel in) {
            return new Organization(in);
        }

        @Override
        public Organization[] newArray(int size) {
            return new Organization[size];
        }
    };

    public String getLogin() {
        return login;
    }

    public String getId() {
        return id;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public String getOrganizations_url() {
        return organizations_url;
    }

    public String getRepos_url() {
        return repos_url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(login);
        dest.writeString(id);
        dest.writeString(avatar_url);
        dest.writeString(organizations_url);
        dest.writeString(repos_url);
    }
}
