package com.masiuk.githubsearch.Models;


import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OrganizationResponse {
    @SerializedName("items")

    private List<Organization> organizations;

    @NonNull
    public List<Organization> getOrganizations() {
        if (organizations == null) {
            return new ArrayList<>();
        }
        return organizations;
    }
}
