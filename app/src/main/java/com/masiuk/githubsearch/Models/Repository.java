package com.masiuk.githubsearch.Models;


import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Repository extends RealmObject{
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("stargazers_count")
    private int stars;
    @SerializedName("watchers")
    private int watchers;
    @SerializedName("forks")
    private int forks;


    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getStars() {
        return stars;
    }

    public int getWatchers() {
        return watchers;
    }

    public int getForks() {
        return forks;
    }
}
