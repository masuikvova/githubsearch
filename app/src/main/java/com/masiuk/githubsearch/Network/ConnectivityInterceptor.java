package com.masiuk.githubsearch.Network;


import com.masiuk.githubsearch.GitHubApp;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ConnectivityInterceptor  implements Interceptor{
    @Override
    public Response intercept(Chain chain) throws IOException {
        if (!NetworkUtil.isOnline(GitHubApp.getInstance())) {
            throw new NoConnectivityException();
        }

        Request.Builder builder = chain.request().newBuilder();
        return chain.proceed(builder.build());
    }
}
