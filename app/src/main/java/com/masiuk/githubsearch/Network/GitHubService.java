package com.masiuk.githubsearch.Network;

import com.masiuk.githubsearch.Models.OrganizationResponse;
import com.masiuk.githubsearch.Models.Repository;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;


public interface GitHubService {
    @GET("search/users?")
    Observable<OrganizationResponse> searchOrganization(@Query("q") String search_text);

    @GET("users/{name}/repos?per_page=100")
    Observable<List<Repository>> getRepositories(@Path("name") String name);

    @GET("users/{name}/repos?per_page=30")
    Observable<List<Repository>> getRepositories(@Path("name") String name, @Query("page") int page);
}
