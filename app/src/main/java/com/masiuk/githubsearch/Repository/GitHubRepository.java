package com.masiuk.githubsearch.Repository;


import com.masiuk.githubsearch.Models.Organization;
import com.masiuk.githubsearch.Models.OrganizationResponse;
import com.masiuk.githubsearch.Models.Repository;
import com.masiuk.githubsearch.Network.APIFactory;
import com.masiuk.githubsearch.Network.NoConnectivityException;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Observable;

public class GitHubRepository {

    public static Observable<List<Organization>> searchOrganization(String search_text) {
        return APIFactory.getGitHubService()
                .searchOrganization(search_text)
                .map(OrganizationResponse::getOrganizations)
                .flatMap(organizations -> {
                    Realm.getDefaultInstance().executeTransaction(realm -> {
                        realm.delete(Organization.class);
                        realm.insert(organizations);
                    });
                    return Observable.just(organizations);
                })
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof NoConnectivityException)
                        return Observable.error(throwable);
                    Realm realm = Realm.getDefaultInstance();
                    RealmResults<Organization> results = realm.where(Organization.class).findAll();
                    return Observable.just(realm.copyFromRealm(results));
                });
    }

    public static Observable<List<Repository>> getRepositories(String name, int page) {
        return APIFactory.getGitHubService()
                .getRepositories(name, page)
                .flatMap(repositories -> {
                    Realm.getDefaultInstance().executeTransaction(realm -> {
                        realm.delete(Repository.class);
                        realm.insert(repositories);
                    });
                    return Observable.just(repositories);
                })
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof NoConnectivityException)
                        return Observable.error(throwable);
                    Realm realm = Realm.getDefaultInstance();
                    RealmResults<Repository> results = realm.where(Repository.class).findAll();
                    return Observable.just(realm.copyFromRealm(results));
                });
    }
}
