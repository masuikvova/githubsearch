package com.masiuk.githubsearch.Screens.ReposScreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v13.view.ViewCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.masiuk.githubsearch.Models.Organization;
import com.masiuk.githubsearch.Models.Repository;
import com.masiuk.githubsearch.R;
import com.masiuk.githubsearch.Screens.Common.PresenterLoader;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RepositoriesActivity extends AppCompatActivity implements RepositoriesView {
    public static final String EXTRA_ORG = "name";
    public static final String IMAGE = "image";
    private static final int LOADER_ID = 2;
    public static final String CONFIG_CHANGE = "config_change";
    @BindView(R.id.rvRepositories)
    RecyclerView rvRepositories;
    @BindView(R.id.flEmptyView)
    FrameLayout emptyView;
    @BindView(R.id.pbLoading)
    ProgressBar loadingView;
    @BindView(R.id.ivAvatar)
    ImageView logoView;
    @BindView(R.id.tvName)
    TextView tvName;
    private RepositoriesPresenter presenter;
    private RepositoriesAdapter adapter;
    private Organization organization;
    private EndlessRecyclerViewScrollListener scrollListener;
    private boolean configurationChanged = false;

    public static void navigate(@NonNull AppCompatActivity activity, @NonNull View transitionImage, @NonNull Organization organization) {
        Intent intent = new Intent(activity, RepositoriesActivity.class);
        intent.putExtra(EXTRA_ORG, organization);

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, IMAGE);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositories);
        ButterKnife.bind(this);

        LoaderManager.LoaderCallbacks<RepositoriesPresenter> callback = new LoaderCallback();
        getSupportLoaderManager().initLoader(LOADER_ID, Bundle.EMPTY, callback);
        organization = getIntent().getParcelableExtra(EXTRA_ORG);
        if (savedInstanceState != null) {
            configurationChanged = savedInstanceState.getBoolean(CONFIG_CHANGE, false);
        }
        initView();
    }

    private void initView() {
        adapter = new RepositoriesAdapter(this, (v, repository) -> InfoDialog.showInfoDialog(this, repository));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvRepositories.setLayoutManager(layoutManager);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(rvRepositories.getContext(),
                layoutManager.getOrientation());
        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager, (page, count, view) -> {
            presenter.loadRepositories(organization.getLogin(), page);
        });
        mDividerItemDecoration.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider));
        rvRepositories.addItemDecoration(mDividerItemDecoration);
        rvRepositories.setAdapter(adapter);
        rvRepositories.addOnScrollListener(scrollListener);

        tvName.setText(organization.getLogin());
        Glide.with(this)
                .load(organization.getAvatar_url())
                .into(logoView);
        ViewCompat.setTransitionName(logoView, IMAGE);

        getSupportActionBar().setTitle(organization.getLogin());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
        if (!configurationChanged)
            presenter.loadRepositories(organization.getLogin());
    }

    @Override
    public void showLoadingView() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingView() {
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void showRepositories(List<Repository> data) {
        if (data.size() > 0) {
            emptyView.setVisibility(View.GONE);
            adapter.setData(data);
            setRepositoriesTitle(data.size());
        } else {
            adapter.clearData();
            getSupportActionBar().setTitle(organization.getLogin());
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    private void setRepositoriesTitle(int size) {
        String title = String.format(getString(R.string.repo_title_template), organization.getLogin(), size);
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void showError() {
        Toast.makeText(this, getString(R.string.repo_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void noConnection() {
        Toast.makeText(this, getString(R.string.no_connection_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(CONFIG_CHANGE, true);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    private class LoaderCallback implements LoaderManager.LoaderCallbacks<RepositoriesPresenter> {
        @Override
        public Loader<RepositoriesPresenter> onCreateLoader(int id, Bundle args) {
            return new PresenterLoader<>(RepositoriesActivity.this, RepositoriesPresenter.class);
        }

        @Override
        public void onLoadFinished(Loader<RepositoriesPresenter> loader, RepositoriesPresenter presenterImp) {
            presenter = presenterImp;
            scrollListener.setCurrentPage(presenter.getCurrentPage());
        }

        @Override
        public void onLoaderReset(Loader<RepositoriesPresenter> loader) {

        }
    }
}
