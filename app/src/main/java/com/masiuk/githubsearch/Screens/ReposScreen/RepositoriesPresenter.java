package com.masiuk.githubsearch.Screens.ReposScreen;

import com.masiuk.githubsearch.Models.Repository;
import com.masiuk.githubsearch.Network.NoConnectivityException;
import com.masiuk.githubsearch.Repository.GitHubRepository;
import com.masiuk.githubsearch.Screens.Common.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class RepositoriesPresenter implements BasePresenter {
    private RepositoriesView repositoriesView;
    private Subscription subscription;
    private List<Repository> repositories = new ArrayList<>();
    private int currentPage = 1;

    public void attachView(RepositoriesView repositoriesView) {
        this.repositoriesView = repositoriesView;
        if (repositories != null && repositories.size() > 0)
            repositoriesView.showRepositories(repositories);
    }

    public void loadRepositories(String name) {
        loadRepositories(name, 1);
    }

    public void loadRepositories(String name, int currentPage) {
        this.currentPage = currentPage;
        subscription = GitHubRepository.getRepositories(name, currentPage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(repositoriesView::showLoadingView)
                .doAfterTerminate(repositoriesView::hideLoadingView)
                .subscribe(this::handleResult,
                        this::handleError);
    }

    private void handleResult(List<Repository> data) {
        repositories.addAll(data);
        repositoriesView.showRepositories(repositories);
    }

    private void handleError(Throwable throwable) {
        if (throwable instanceof NoConnectivityException)
            repositoriesView.noConnection();
        else
            repositoriesView.showError();
    }

    public void detachView() {
        if (subscription != null)
            subscription.unsubscribe();
        this.repositoriesView = null;
    }

    public int getCurrentPage() {
        return currentPage;
    }
}
