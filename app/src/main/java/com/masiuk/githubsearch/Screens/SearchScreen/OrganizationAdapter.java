package com.masiuk.githubsearch.Screens.SearchScreen;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.masiuk.githubsearch.Models.Organization;
import com.masiuk.githubsearch.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class OrganizationAdapter extends RecyclerView.Adapter<OrganizationAdapter.OrganizationHolder> {
    private List<Organization> data = new ArrayList<>();
    private Context context;

    private final OnItemClickListener mOnItemClickListener;

    private final View.OnClickListener mInternalListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Organization organization = (Organization) view.getTag();
            mOnItemClickListener.onItemClick(view, organization);
        }
    };

    public OrganizationAdapter(Context context, @NonNull OnItemClickListener onItemClickListener) {
        this.context = context;
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public OrganizationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.item_organization, null);
        return new OrganizationHolder(view);
    }

    @Override
    public void onBindViewHolder(OrganizationHolder holder, int position) {
        Organization item = data.get(position);
        holder.tvName.setText(item.getLogin());
        holder.tvLink.setText(item.getOrganizations_url());
        holder.tvSubname.setText(item.getId());
        holder.itemView.setOnClickListener(mInternalListener);
        holder.itemView.setTag(item);
        setImage(holder.ivAvatar, item.getAvatar_url());
    }

    private void setImage(final ImageView image, String url) {
        Glide.with(context)
                .load(url)
                .into(image);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<Organization> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void clearData() {
        data.clear();
        notifyDataSetChanged();
    }

    class OrganizationHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivAvatar)
        ImageView ivAvatar;
        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvSubName)
        TextView tvSubname;
        @BindView(R.id.tvLink)
        TextView tvLink;

        public OrganizationHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {

        void onItemClick(@NonNull View view, @NonNull Organization organization);
    }
}
