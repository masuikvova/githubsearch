package com.masiuk.githubsearch.Screens.ReposScreen;

import com.masiuk.githubsearch.Models.Repository;
import com.masiuk.githubsearch.Screens.Common.BaseView;

import java.util.List;


public interface RepositoriesView extends BaseView {
    void showLoadingView();

    void hideLoadingView();

    void showRepositories(List<Repository> data);
}
