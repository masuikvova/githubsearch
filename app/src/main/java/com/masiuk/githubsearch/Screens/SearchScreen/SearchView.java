package com.masiuk.githubsearch.Screens.SearchScreen;

import com.masiuk.githubsearch.Models.Organization;
import com.masiuk.githubsearch.Screens.Common.BaseView;

import java.util.List;


public interface SearchView extends BaseView{
    void showOrganization(List<Organization> data);
}
