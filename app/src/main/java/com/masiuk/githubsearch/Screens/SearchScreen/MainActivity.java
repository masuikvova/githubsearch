package com.masiuk.githubsearch.Screens.SearchScreen;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.masiuk.githubsearch.Models.Organization;
import com.masiuk.githubsearch.R;
import com.masiuk.githubsearch.Screens.Common.PresenterLoader;
import com.masiuk.githubsearch.Screens.ReposScreen.RepositoriesActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements SearchView {
    private static final int LOADER_ID = 1;
    @BindView(R.id.rvOrganization)
    RecyclerView organizationsList;
    @BindView(R.id.flEmptyView)
    FrameLayout emptyView;
    @BindView(R.id.SearchField)
    EditText searchField;
    private SearchPresenter presenter;
    private OrganizationAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        LoaderManager.LoaderCallbacks<SearchPresenter> callback = new LoaderCallback();
        getSupportLoaderManager().initLoader(LOADER_ID, Bundle.EMPTY, callback);
        initView();
    }

    private void initView() {
        adapter = new OrganizationAdapter(this, (view, organization) ->
                RepositoriesActivity.navigate(this, view.findViewById(R.id.ivAvatar),organization)
        );
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        organizationsList.setLayoutManager(layoutManager);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(organizationsList.getContext(),
                layoutManager.getOrientation());
        mDividerItemDecoration.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider));
        organizationsList.addItemDecoration(mDividerItemDecoration);
        organizationsList.setAdapter(adapter);

        searchField.addTextChangedListener(new SearchTextWatcher());
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
    }

    @Override
    public void showOrganization(List<Organization> data) {
        if (data.size() > 0) {
            emptyView.setVisibility(View.GONE);
            adapter.setData(data);
        } else {
            adapter.clearData();
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showError() {
        Toast.makeText(this, getString(R.string.search_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void noConnection() {
        Toast.makeText(this, getString(R.string.no_connection_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    private class SearchTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() >= 3) {
                presenter.onSearchTextChange(s.toString());
            } else {
                adapter.clearData();
            }
        }
    }

    private class LoaderCallback implements LoaderManager.LoaderCallbacks<SearchPresenter> {
        @Override
        public Loader<SearchPresenter> onCreateLoader(int id, Bundle args) {
            return new PresenterLoader<>(MainActivity.this, SearchPresenter.class);
        }

        @Override
        public void onLoadFinished(Loader<SearchPresenter> loader, SearchPresenter presenterImp) {
            presenter = presenterImp;
        }

        @Override
        public void onLoaderReset(Loader<SearchPresenter> loader) {

        }
    }
}
