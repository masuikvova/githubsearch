package com.masiuk.githubsearch.Screens.ReposScreen;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.masiuk.githubsearch.Models.Repository;
import com.masiuk.githubsearch.R;


public class InfoDialog {
    private static final int ANIMATION_TIME = 800;

    public static void showInfoDialog(Activity activity, Repository repository) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_info);

        ImageView ivLogo = (ImageView) dialog.findViewById(R.id.ivLogo);
        ivLogo.setImageResource(R.drawable.logo);

        ObjectAnimator imageAnimator = ObjectAnimator.ofFloat(ivLogo, "rotationY", 0.0f, 360f);
        imageAnimator.setDuration(ANIMATION_TIME);
        imageAnimator.setInterpolator(new AccelerateDecelerateInterpolator());

        ((TextView) dialog.findViewById(R.id.tvRepoTitle)).setText(repository.getName());
        ((TextView) dialog.findViewById(R.id.tvWatchCount)).setText(String.valueOf(repository.getWatchers()));
        ((TextView) dialog.findViewById(R.id.tvStarCount)).setText(String.valueOf(repository.getStars()));
        ((TextView) dialog.findViewById(R.id.tvForkCount)).setText(String.valueOf(repository.getForks()));

        dialog.show();
        imageAnimator.start();
    }
}
