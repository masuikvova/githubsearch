package com.masiuk.githubsearch.Screens.ReposScreen;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.masiuk.githubsearch.Models.Repository;
import com.masiuk.githubsearch.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.RepositoryHolder> {

    private List<Repository> data = new ArrayList<>();
    private Context context;
    private final OnItemClickListener onItemClickListener;

    private final View.OnClickListener mInternalListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Repository repository = (Repository) view.getTag();
            onItemClickListener.onItemClick(view, repository);
        }
    };

    public RepositoriesAdapter(Context context, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public RepositoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.item_repository, null);
        return new RepositoryHolder(view);
    }

    @Override
    public void onBindViewHolder(RepositoryHolder holder, int position) {
        Repository item = data.get(position);
        holder.tvTitle.setText(item.getName());
        holder.tvDescription.setText(item.getDescription() == null ? context.getString(R.string.repo_no_description) : item.getDescription());
        holder.itemView.setOnClickListener(mInternalListener);
        holder.itemView.setTag(item);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<Repository> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void clearData() {
        data.clear();
    }

    class RepositoryHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvDescription)
        TextView tvDescription;

        public RepositoryHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(@NonNull View view, @NonNull Repository repository);
    }
}
