package com.masiuk.githubsearch.Screens.Common;


import android.content.Context;
import android.support.v4.content.Loader;

public class PresenterLoader<T extends BasePresenter> extends Loader<T> {
    private T presenter;
    private Class<T> aClass;

    public PresenterLoader(Context context, Class<T> aClass) {

        super(context);
        this.aClass = aClass;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if (presenter == null) {
            try {
                presenter = aClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        deliverResult(presenter);
    }
}
