package com.masiuk.githubsearch.Screens.SearchScreen;


import com.masiuk.githubsearch.Models.Organization;
import com.masiuk.githubsearch.Network.NoConnectivityException;
import com.masiuk.githubsearch.Repository.GitHubRepository;
import com.masiuk.githubsearch.Screens.Common.BasePresenter;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SearchPresenter implements BasePresenter {
    private SearchView searchView;
    private Subscription subscription;
    private List<Organization> organizations;

    public void attachView(SearchView searchView) {
        this.searchView = searchView;
        if (organizations != null && organizations.size() > 0)
            searchView.showOrganization(organizations);
    }

    public void detachView() {
        if (subscription != null)
            subscription.unsubscribe();
        this.searchView = null;
    }

    public void onSearchTextChange(String newText) {
        subscription = GitHubRepository.searchOrganization(newText)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleResult, this::handleError);
    }

    private void handleResult(List<Organization> data) {
        organizations = data;
        searchView.showOrganization(organizations);
    }

    private void handleError(Throwable throwable) {
        if (throwable instanceof NoConnectivityException)
            searchView.noConnection();
        else
            searchView.showError();
    }

}
