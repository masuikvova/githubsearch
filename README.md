# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ##

The solved test task.(Search organization on github and show all repositories)

## Libraries and tools used

* Android Support Libraries
* RxJava2
* ButterKnife
* Glide
* Retrofit
* OkHttp
* Gson
* Realm
* Retrolambda

## Screens

![ScreenShot](/app/screen/screen_1.png?=250x250)
![ScreenShot](/app/screen/screen_2.png?=250x250)
![ScreenShot](/app/screen/screen_3.png?=250x250)




